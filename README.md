# TTM00-FW-PIC32-Training-Week2

This repo is for training purposes

1. Project Brief
    This project is for training purposes from the Titoma Company and is about different topics related to the programming of microcontroller STM32 (Week 3).
2. Hardware Description
    - STM32F429ZIT: "https://www.st.com/resource/en/datasheet/dm00071990.pdf"    
3. Serial commands
4. Prerequisites
5. Versioning
    1. Current version of the FW
        - V1.0.20210727
6. Authors
    1. Project staff: "Miguel Angel Arias Giraldo"
    2. Maintainer contact email: "miguelariasgiraldo2000@gmail.com"